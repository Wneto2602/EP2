/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author renato
 */
public class Estoque extends javax.swing.JInternalFrame {

    String nomeproduto;
    String quantproduto;
    /**
     * Creates new form Estoque
     */
    public Estoque() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jtnomeproduto = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jtquantproduto = new javax.swing.JTextField();
        jbsalvar = new javax.swing.JButton();
        jblimpar = new javax.swing.JButton();
        jbexcluir = new javax.swing.JButton();
        jvisualizar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Estoque de Produtos");
        setName("estoque"); // NOI18N

        jLabel1.setText("Tipo:");

        jtnomeproduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtnomeprodutoActionPerformed(evt);
            }
        });

        jLabel2.setText("Quantidade:");

        jbsalvar.setText("Salvar");
        jbsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbsalvarActionPerformed(evt);
            }
        });

        jblimpar.setText("Limpar");
        jblimpar.setEnabled(false);
        jblimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jblimparActionPerformed(evt);
            }
        });

        jbexcluir.setText("Excluir");
        jbexcluir.setEnabled(false);
        jbexcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbexcluirActionPerformed(evt);
            }
        });

        jvisualizar.setText("Visualizar");
        jvisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jvisualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jtquantproduto, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jtnomeproduto, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jbsalvar, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jblimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jbexcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jvisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(35, 35, 35))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jtnomeproduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jtquantproduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbsalvar)
                    .addComponent(jblimpar)
                    .addComponent(jbexcluir)
                    .addComponent(jvisualizar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtnomeprodutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtnomeprodutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtnomeprodutoActionPerformed

    private void jbexcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbexcluirActionPerformed

       
        // TODO add your handling code here:
    }//GEN-LAST:event_jbexcluirActionPerformed

    private void jblimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jblimparActionPerformed

        jtnomeproduto.setText("");
        jtquantproduto.setText("");
        

        // TODO add your handling code here:
    }//GEN-LAST:event_jblimparActionPerformed

    private void jbsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbsalvarActionPerformed

         nomeproduto  = jtnomeproduto.getText();
            quantproduto = jtquantproduto.getText();
    
            try { 
           PrintWriter arq = new PrintWriter(jtnomeproduto.getText()+".txt"); 
           arq.println(jtnomeproduto.getText());
           arq.println(jtquantproduto.getText());
           arq.close();
           JOptionPane.showMessageDialog(null,"Produto Salvo");
           
            
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null,"Produto Não Salvo"+erro.getMessage());
           
        }
  




        // TODO add your handling code here:
    }//GEN-LAST:event_jbsalvarActionPerformed

    private void jvisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jvisualizarActionPerformed

        
        jbexcluir.setEnabled(true);
        jblimpar.setEnabled(true);
        
        
        
        
        
        
        try {
            String nomeproduto= JOptionPane.showInputDialog("Digite nome do Produto:");
            BufferedReader arq = new BufferedReader(new FileReader(nomeproduto+".txt"));
            jtnomeproduto.setText(arq.readLine());
            jtquantproduto.setText(arq.readLine());
            arq.close();
            // TODO add your handling code here:
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null,"Error:"+erro.getMessage());
        }
    }//GEN-LAST:event_jvisualizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton jbexcluir;
    private javax.swing.JButton jblimpar;
    private javax.swing.JButton jbsalvar;
    private javax.swing.JTextField jtnomeproduto;
    private javax.swing.JTextField jtquantproduto;
    private javax.swing.JButton jvisualizar;
    // End of variables declaration//GEN-END:variables
}
